const express = require('express')

const app = express();

const port = 3001;

app.use(express.json());
app.use(express.urlencoded({ extended:true }));

// RETURN SIMPLE MESSAGE
app.get('/home', (request, response) => {
	response.send('Welcome to the home page!');
})


// RETRIVE ALL THE USERS
let users = [
	    {
	        "username" : 'johndoe', 
	        "password" : 'johndoe123'
	    },
	    {
	        "username" : 'doejane', 
	        "password" : 'jane789'
	    }

    ];

app.post('/signup', (request, response) => {
	response.send("You are successfully signup!");
});

app.get('/users', (request, response) => {
	response.send((users));
});


// DELETE RESOURCE
app.delete('/delete-user', (request,response) => {
	response.send("The users has been deleted!");
})



app.listen(port, () => console.log (`Server running at port: ${port}`));


